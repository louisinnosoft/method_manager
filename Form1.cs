﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Innosoft;
using System.IO;
using Aras.IOM;
using System.Xml;

/*
 * 原始碼管控
 * 1.一律匯出全部,只要匯出一邊
 * 2.會出來的資料夾不需押上日期,反而是要押上類型(method, server event....)
 * 
 */


namespace MethodManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Innosoft.InnovatorHelper InnH_From;
        Innosoft.InnovatorHelper InnH_To;
        bool IsFromLoginOk = false;
        bool IsToLoginOk = false;
        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnFromLogin_Click(object sender, EventArgs e)
        {
			
            picFromNG.Visible = false;
            picFromOk.Visible = false;
            string strMessage = "";
            try
            {

                InnH_From = new InnovatorHelper(txtFromURL.Text,txtFromLoginId.Text,txtFromPwd.Text,txtFromDb.Text, out strMessage);
                if (strMessage !="ok")
                {
                    MessageBox.Show(strMessage);
                    picFromNG.Visible = true;
                }
                else
                {
                    picFromOk.Visible = true;
                    IsFromLoginOk = true;
                }
                  

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                picFromNG.Visible = true;
            }
        }

        private void btnToLogin_Click(object sender, EventArgs e)
        {
            picToNg.Visible = false;
            picToOk.Visible = false;
            string strMessage = "";
            try
            {

                InnH_To = new InnovatorHelper(txtUrl.Text, txtLoginId.Text, txtPwd.Text, txtDb.Text, out strMessage);
                if (strMessage != "ok")
                {
                    MessageBox.Show(strMessage);
                    picToNg.Visible = true;
                }
                else
                {
                    picToOk.Visible = true;
                    IsToLoginOk = true;
                }
                   

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                picToNg.Visible = true;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            txtMessage.Text = "";
            SaveConfig();
            if (InnH_From != null)
            {
                InnH_From.Logout();
            }
            if (InnH_To != null)
            {
                InnH_To.Logout();
            }
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (Control c in tabControl1.Controls[0].Controls)
            {
                if (c is TextBox)
                {
                    
                    string strName = (c as TextBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "");
                    (c as TextBox).Text = strValue;
                    continue;
                }

                if (c is CheckBox)
                {

                    string strName = (c as CheckBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "false");
                    (c as CheckBox).Checked =Boolean.Parse(strValue);
                    continue;

                }
            }

            foreach (Control c in tabControl2.Controls[0].Controls)
            {
                if (c is TextBox)
                {

                    string strName = (c as TextBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "");
                    (c as TextBox).Text = strValue;

                }

                if (c is CheckBox)
                {

                    string strName = (c as CheckBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "false");
                    (c as CheckBox).Checked = Boolean.Parse(strValue);
                    continue;

                }
            }

            foreach (Control c in tableLayoutPanel1.Controls)
            {
                if (c is TextBox)
                {

                    string strName = (c as TextBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "");
                    (c as TextBox).Text = strValue;

                }

                if (c is CheckBox)
                {

                    string strName = (c as CheckBox).Name;
                    string strValue = InnUtility.GetSettingValue("In_MethodManager", strName, "false");
                    (c as CheckBox).Checked = Boolean.Parse(strValue);
                    continue;

                }
            }

           

            string strdtModified_on = InnUtility.GetSettingValue("In_MethodManager", "dtModified_on", "");
            if (strdtModified_on == "")
                dtModified_on.Value = System.DateTime.Now;
            else
                dtModified_on.Value = System.DateTime.Parse(strdtModified_on);


            picFromNG.Visible = false;
            picFromOk.Visible = false;
            picToOk.Visible = false;
            picToNg.Visible = false;

            string strExportDir = txtExportFolder.Text;
            if(!Directory.Exists(strExportDir))
                txtExportFolder.Text = Application.StartupPath;

        }

        private void SaveConfig()
        {
            
            foreach (Control c in tabControl1.Controls[0].Controls)
            {
                if (c is TextBox)
                {
                    string strValue = (c as TextBox).Text;
                    string strName = (c as TextBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }

                if (c is CheckBox)
                {
                    string strValue = (c as CheckBox).Checked.ToString();
                    string strName = (c as CheckBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }
            }

            foreach (Control c in tabControl2.Controls[0].Controls)
            {
                if (c is TextBox)
                {
                    string strValue = (c as TextBox).Text;
                    string strName = (c as TextBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }

                if (c is CheckBox)
                {
                    string strValue = (c as CheckBox).Checked.ToString();
                    string strName = (c as CheckBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }
            }

            foreach (Control c in tableLayoutPanel1.Controls)
            {
                if (c is TextBox)
                {
                    string strValue = (c as TextBox).Text;
                    string strName = (c as TextBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }

                if (c is CheckBox)
                {
                    string strValue = (c as CheckBox).Checked.ToString();
                    string strName = (c as CheckBox).Name;
                    InnUtility.SetSettingValue("In_MethodManager", strName, strValue);
                    continue;
                }
            }

            string strdtModified_on = dtModified_on.Value.ToString("yyyy-MM-ddTHH:mm:ss");
            InnUtility.SetSettingValue("In_MethodManager", "dtModified_on", strdtModified_on);




        }

        private void btnSelectExportFolder_Click(object sender, EventArgs e)
        {
            if (txtExportFolder.Text != "")
                folderBrowserDialog1.SelectedPath = txtExportFolder.Text;
            else
                folderBrowserDialog1.SelectedPath = Application.StartupPath;
            folderBrowserDialog1.ShowDialog();
            txtExportFolder.Text = folderBrowserDialog1.SelectedPath;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if(!(IsFromLoginOk && IsToLoginOk))
            {
                MessageBox.Show("請先登入來源與目的端的資料庫");
                return;
            }

            SaveConfig();
            


            //以來源的METHOD為基礎尋找被更新端的METHOD
            //移除空白字元之後如果有相異則匯出文字檔
            lbTask.Text = "比對並匯出程式碼";
            txtMessage.Text = "";
            string strFolderName = txtExportFolder.Text + @"\" + System.DateTime.Now.ToString("MMddHHmm");
            txtFromSource.Text = strFolderName + @"\from_" + txtFromDb.Text;
            txtToSource.Text = strFolderName + @"\to_" + txtDb.Text;
            txtTobeUpdate.Text = strFolderName + @"\patch";

            Item itmFromMetohds = InnH_From.getInn().newItem("Method");
            string strModifiedOn = dtModified_on.Value.ToString("yyyy-MM-ddTHH:mm:ss");
            itmFromMetohds.setProperty("modified_on", strModifiedOn);
            itmFromMetohds.setPropertyCondition("modified_on", "ge");
            itmFromMetohds = itmFromMetohds.apply("get");
            progressBar1.Maximum = itmFromMetohds.getItemCount();
            progressBar1.Value = 0;
            int intSame = 0;
            int intDiff = 0;
            int intNew = 0;
           
            for (int i=0;i<itmFromMetohds.getItemCount();i++)
            {
                string strResult = "";
                Item itmFromMethod = itmFromMetohds.getItemByIndex(i);
                string strName = itmFromMethod.getProperty("keyed_name");
                string strFromMethod = "";
                string strToMethod = "";
                string strFromMethod_WOSpace = "";
                string strToMethod_WOSpace = "";

                strFromMethod_WOSpace = itmFromMethod.getProperty("method_code", "");
                strFromMethod_WOSpace = RemoveWhitespace(strFromMethod_WOSpace);

                string strVer = itmFromMethod.getProperty("in_ver", "");
                string strVerNote = itmFromMethod.getProperty("in_ver_note", "");
                string strComments = itmFromMethod.getProperty("comments", "");

                Item itmToMethod = InnH_To.getInn().getItemByKeyedName("Method", strName);

                if(itmToMethod==null || itmToMethod.isError())
                {
                    strResult = "new";
                    intNew++;
                }
                else
                {
                    strToMethod_WOSpace = itmToMethod.getProperty("method_code", "");
                    strToMethod_WOSpace = RemoveWhitespace(strToMethod_WOSpace);
                    if (strFromMethod_WOSpace == strToMethod_WOSpace)
                    {
                        strResult = "same";
                        intSame++;
                    }
                    else
                    {
                        strResult = "diff";
                        intDiff++;
                    }
                }

                //無論比對狀態如何都應該將來源與目的地的純檔案匯出
                if(chkExportAll.Checked)
                {
                    strFromMethod = GetMethodContext(itmFromMethod);
                    strToMethod = GetMethodContext(itmToMethod);
                    InnUtility.WriteTxtFile(strFolderName + @"\from_" + txtFromDb.Text + "_Ori", strName + ".txt", strFromMethod, false, true);
                    InnUtility.WriteTxtFile(strFolderName + @"\to_" + txtDb.Text + "_Ori", strName + ".txt", strToMethod, false, true);

                }

                if (strResult!="same")
                {
                    if(strFromMethod=="")
                        strFromMethod = GetMethodContext(itmFromMethod);
                    InnUtility.WriteTxtFile(strFolderName + @"\from_" + txtFromDb.Text, strName + ".txt", strFromMethod, false, true);
                    InnUtility.WriteTxtFile(strFolderName + @"\patch", strName + ".txt", strFromMethod, false, true);

                    if(strResult=="diff")
                    {
                        if (strToMethod == "")
                            strToMethod = GetMethodContext(itmToMethod);
                        InnUtility.WriteTxtFile(strFolderName + @"\to_" + txtDb.Text, strName + ".txt", strToMethod, false, true);
                    }
                }

                Application.DoEvents();
                progressBar1.Value = i+1;
                InnUtility.AddLog("[" + strResult + "]-" + (i+1).ToString() + "/" + itmFromMetohds.getItemCount().ToString() + "-" + strName, "info", txtMessage);


            }
            string strFinalResult = "total:" + itmFromMetohds.getItemCount().ToString() + ",new:" + intNew.ToString() + ",diff:" + intDiff.ToString() + ",same:" + intSame.ToString();
            InnUtility.AddLog(strFinalResult, "info", txtMessage);



        }

        public string GetMethodContext(Item Method)
        {
            string strMethodCode = "";
            strMethodCode = Method.getProperty("method_code", "");
            StringBuilder sb = new StringBuilder();
            sb.Append(strMethodCode);
            sb.Append("\n#innosoft#");
            sb.Append("\n<AML>");
            sb.Append("\n<Item type='Method' where=\"[Method].[Name]='" + Method.getProperty("name") + "' and [Method].is_current='1'\">");
            sb.Append("\n<config_id>" + Method.getProperty("config_id", "") + "</config_id>");
            sb.Append("\n<name>" + Method.getProperty("name") + "</name>");
            sb.Append("\n<comments>" + System.Security.SecurityElement.Escape(Method.getProperty("comments")) + "</comments>");
            sb.Append("\n<in_ver>" + Method.getProperty("in_ver", "") + "</in_ver>");
            sb.Append("\n<in_ver_note>" + Method.getProperty("in_ver_note", "") + "</in_ver_note>");
            sb.Append("\n<method_type>" + Method.getProperty("method_type") + "</method_type>");
            sb.Append("\n</Item>");
            sb.Append("\n</AML>");
            strMethodCode = sb.ToString();

            return strMethodCode;
        }

        public  string RemoveWhitespace(string str)
        {
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        private void btnGenTobeUpdate_Click(object sender, EventArgs e)
        {
            
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (!(IsFromLoginOk && IsToLoginOk))
            {
                MessageBox.Show("請先登入來源與目的端的資料庫");
                return;
            }

            SaveConfig();
            int intAdd = 0;
            int intEdit = 0;
            int intError = 0;
            int intIgnore = 0;

            txtMessage.Text = "";
            lbTask.Text = "實際匯入";
            DirectoryInfo dirPatch = new DirectoryInfo(txtTobeUpdate.Text);
            FileInfo[] PatchInfos = dirPatch.GetFiles("*.txt");
            int i = 0;
            progressBar1.Maximum = PatchInfos.Length;
            progressBar1.Value = 0;
            foreach (FileInfo PatchInfo in PatchInfos)
            {
                string PatchContent =  File.ReadAllText(PatchInfo.FullName, Encoding.UTF8);
                string MethodName = PatchInfo.Name.Substring(0, PatchInfo.Name.LastIndexOf(PatchInfo.Extension));
                string strTag = "#innosoft#";
                int TagPos = PatchContent.IndexOf(strTag);
                string MethodCode = PatchContent.Substring(0, TagPos);
                string MethodAML = PatchContent.Substring(TagPos + strTag.Length);

                Item itmExistMethod = InnH_To.getInn().getItemByKeyedName("Method", MethodName);
                string strAction = "";
                if(itmExistMethod==null)
                {
                    strAction = "add";
                    intAdd++;
                }
                else
                {
                    if(itmExistMethod.isError())
                    {
                        intError++;
                        InnUtility.AddLog("[error]-" + itmExistMethod.getErrorString() + ",AML:" + MethodAML, "info", txtMessage);
                        continue;
                    }
                    else
                    {
                        strAction = "edit";
                        if (itmExistMethod.fetchLockStatus() != 0)
                            itmExistMethod.unlockItem();
                        intEdit++;
                    }
                    

                }
                Item itmMethod = InnH_To.getInn().newItem();

                Application.DoEvents();
                i++;
                progressBar1.Value = i;
                InnUtility.AddLog("[" + strAction + "]-" + i.ToString() + "/" + PatchInfos.Length.ToString() + "-" + MethodName, "info", txtMessage);
                itmMethod.loadAML(MethodAML);
                itmMethod.setAction("in_merge");
                itmMethod.setProperty("method_code", MethodCode);
                if (itmExistMethod != null)
                {
                    //如果Method內容一模一樣,則不要異動,以免破壞last modified date
                    string ExistMethodCode = itmExistMethod.getProperty("method_code", "");
                    ExistMethodCode = RemoveWhitespace(ExistMethodCode);
                    MethodCode = RemoveWhitespace(MethodCode);
                    if (ExistMethodCode == MethodCode)
                    {
                        intIgnore++;
                        InnUtility.AddLog("[ignore]-" + itmExistMethod.getProperty("keyed_name") + ",AML:" + MethodAML, "info", txtMessage);
                        continue;
                    }
                }
                

                Item itmResult =  itmMethod.apply();
                if(itmResult.isError())
                {
                    intError++;
                    InnUtility.AddLog("[error]-" + itmResult.getErrorString() + ",AML:" + itmMethod.dom.InnerXml, "info", txtMessage);
                }

            }
            InnUtility.AddLog("error:" + intError.ToString() + ",add:" + intAdd.ToString() + ",edit:" + intEdit.ToString() + ",ignore:" + intIgnore.ToString(), "info", txtMessage);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtMessage.Text = "";
        }
    }
}

﻿namespace MethodManager
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkExportAll = new System.Windows.Forms.CheckBox();
            this.picToNg = new System.Windows.Forms.PictureBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.picToOk = new System.Windows.Forms.PictureBox();
            this.btnSelectExportFolder = new System.Windows.Forms.Button();
            this.txtExportFolder = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dtModified_on = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.picFromNG = new System.Windows.Forms.PictureBox();
            this.picFromOk = new System.Windows.Forms.PictureBox();
            this.btnFromLogin = new System.Windows.Forms.Button();
            this.txtFromDb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFromPwd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFromURL = new System.Windows.Forms.TextBox();
            this.txtFromLoginId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtDb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtLoginId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.txtVerNote = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtVer = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFromSource = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtToSource = new System.Windows.Forms.TextBox();
            this.txtTobeUpdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lstTobeUpdated = new System.Windows.Forms.CheckedListBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnGenTobeUpdate = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.lbTask = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToNg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToOk)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFromNG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromOk)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(922, 566);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 22;
            this.button1.Text = "清除";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(13, 605);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessage.Size = new System.Drawing.Size(984, 125);
            this.txtMessage.TabIndex = 19;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1009, 549);
            this.tabControl1.TabIndex = 23;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chkExportAll);
            this.tabPage1.Controls.Add(this.picToNg);
            this.tabPage1.Controls.Add(this.btnExport);
            this.tabPage1.Controls.Add(this.picToOk);
            this.tabPage1.Controls.Add(this.btnSelectExportFolder);
            this.tabPage1.Controls.Add(this.txtExportFolder);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.btnLogin);
            this.tabPage1.Controls.Add(this.txtDb);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtPwd);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtUrl);
            this.tabPage1.Controls.Add(this.txtLoginId);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1001, 517);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "環境設定";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkExportAll
            // 
            this.chkExportAll.AutoSize = true;
            this.chkExportAll.Location = new System.Drawing.Point(162, 478);
            this.chkExportAll.Name = "chkExportAll";
            this.chkExportAll.Size = new System.Drawing.Size(178, 22);
            this.chkExportAll.TabIndex = 67;
            this.chkExportAll.Text = "匯出雙方總程式碼";
            this.chkExportAll.UseVisualStyleBackColor = true;
            // 
            // picToNg
            // 
            this.picToNg.Image = global::MethodManager.Resource1.ng;
            this.picToNg.Location = new System.Drawing.Point(829, 314);
            this.picToNg.Name = "picToNg";
            this.picToNg.Size = new System.Drawing.Size(88, 70);
            this.picToNg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picToNg.TabIndex = 66;
            this.picToNg.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(18, 457);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(123, 43);
            this.btnExport.TabIndex = 59;
            this.btnExport.Text = "開始匯出";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // picToOk
            // 
            this.picToOk.Image = global::MethodManager.Resource1.ok;
            this.picToOk.Location = new System.Drawing.Point(829, 314);
            this.picToOk.Name = "picToOk";
            this.picToOk.Size = new System.Drawing.Size(88, 70);
            this.picToOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picToOk.TabIndex = 65;
            this.picToOk.TabStop = false;
            // 
            // btnSelectExportFolder
            // 
            this.btnSelectExportFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectExportFolder.Location = new System.Drawing.Point(815, 404);
            this.btnSelectExportFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectExportFolder.Name = "btnSelectExportFolder";
            this.btnSelectExportFolder.Size = new System.Drawing.Size(112, 34);
            this.btnSelectExportFolder.TabIndex = 58;
            this.btnSelectExportFolder.Text = "選擇資料夾";
            this.btnSelectExportFolder.UseVisualStyleBackColor = true;
            this.btnSelectExportFolder.Click += new System.EventHandler(this.btnSelectExportFolder_Click);
            // 
            // txtExportFolder
            // 
            this.txtExportFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExportFolder.Location = new System.Drawing.Point(153, 410);
            this.txtExportFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtExportFolder.Name = "txtExportFolder";
            this.txtExportFolder.Size = new System.Drawing.Size(645, 29);
            this.txtExportFolder.TabIndex = 57;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(18, 410);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 28);
            this.label11.TabIndex = 56;
            this.label11.Text = "輸出位置";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(23, 46);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(951, 197);
            this.tabControl2.TabIndex = 55;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dtModified_on);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.picFromNG);
            this.tabPage3.Controls.Add(this.picFromOk);
            this.tabPage3.Controls.Add(this.btnFromLogin);
            this.tabPage3.Controls.Add(this.txtFromDb);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.txtFromPwd);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txtFromURL);
            this.tabPage3.Controls.Add(this.txtFromLoginId);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(943, 165);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "資料庫";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dtModified_on
            // 
            this.dtModified_on.Location = new System.Drawing.Point(115, 120);
            this.dtModified_on.Name = "dtModified_on";
            this.dtModified_on.Size = new System.Drawing.Size(200, 29);
            this.dtModified_on.TabIndex = 66;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 18);
            this.label19.TabIndex = 65;
            this.label19.Text = "最後異動日";
            // 
            // picFromNG
            // 
            this.picFromNG.Image = global::MethodManager.Resource1.ng;
            this.picFromNG.Location = new System.Drawing.Point(812, 56);
            this.picFromNG.Name = "picFromNG";
            this.picFromNG.Size = new System.Drawing.Size(88, 70);
            this.picFromNG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFromNG.TabIndex = 64;
            this.picFromNG.TabStop = false;
            // 
            // picFromOk
            // 
            this.picFromOk.Image = global::MethodManager.Resource1.ok;
            this.picFromOk.Location = new System.Drawing.Point(812, 56);
            this.picFromOk.Name = "picFromOk";
            this.picFromOk.Size = new System.Drawing.Size(88, 70);
            this.picFromOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFromOk.TabIndex = 63;
            this.picFromOk.TabStop = false;
            // 
            // btnFromLogin
            // 
            this.btnFromLogin.Location = new System.Drawing.Point(666, 72);
            this.btnFromLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnFromLogin.Name = "btnFromLogin";
            this.btnFromLogin.Size = new System.Drawing.Size(112, 34);
            this.btnFromLogin.TabIndex = 62;
            this.btnFromLogin.Text = "測試登入";
            this.btnFromLogin.UseVisualStyleBackColor = true;
            this.btnFromLogin.Click += new System.EventHandler(this.btnFromLogin_Click);
            // 
            // txtFromDb
            // 
            this.txtFromDb.Location = new System.Drawing.Point(733, 17);
            this.txtFromDb.Margin = new System.Windows.Forms.Padding(4);
            this.txtFromDb.Name = "txtFromDb";
            this.txtFromDb.Size = new System.Drawing.Size(148, 29);
            this.txtFromDb.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(663, 32);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 18);
            this.label7.TabIndex = 60;
            this.label7.Text = "資料庫";
            // 
            // txtFromPwd
            // 
            this.txtFromPwd.Location = new System.Drawing.Point(394, 72);
            this.txtFromPwd.Margin = new System.Windows.Forms.Padding(4);
            this.txtFromPwd.Name = "txtFromPwd";
            this.txtFromPwd.PasswordChar = '*';
            this.txtFromPwd.Size = new System.Drawing.Size(148, 29);
            this.txtFromPwd.TabIndex = 59;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(341, 86);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 58;
            this.label8.Text = "密碼";
            // 
            // txtFromURL
            // 
            this.txtFromURL.Location = new System.Drawing.Point(112, 17);
            this.txtFromURL.Margin = new System.Windows.Forms.Padding(4);
            this.txtFromURL.Name = "txtFromURL";
            this.txtFromURL.Size = new System.Drawing.Size(540, 29);
            this.txtFromURL.TabIndex = 56;
            // 
            // txtFromLoginId
            // 
            this.txtFromLoginId.Location = new System.Drawing.Point(115, 72);
            this.txtFromLoginId.Margin = new System.Windows.Forms.Padding(4);
            this.txtFromLoginId.Name = "txtFromLoginId";
            this.txtFromLoginId.Size = new System.Drawing.Size(148, 29);
            this.txtFromLoginId.TabIndex = 57;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 32);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 18);
            this.label9.TabIndex = 54;
            this.label9.Text = "網址";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 88);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 18);
            this.label10.TabIndex = 55;
            this.label10.Text = "登入帳號";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button2);
            this.tabPage4.Controls.Add(this.textBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(943, 165);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "資料夾";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(824, 23);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 34);
            this.button2.TabIndex = 60;
            this.button2.Text = "選擇資料夾";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Location = new System.Drawing.Point(7, 23);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(809, 29);
            this.textBox5.TabIndex = 59;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(18, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 28);
            this.label5.TabIndex = 54;
            this.label5.Text = "來源環境";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(22, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 28);
            this.label6.TabIndex = 54;
            this.label6.Text = "被更新的目標環境";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(693, 333);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(112, 34);
            this.btnLogin.TabIndex = 53;
            this.btnLogin.Text = "測試登入";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnToLogin_Click);
            // 
            // txtDb
            // 
            this.txtDb.Location = new System.Drawing.Point(741, 278);
            this.txtDb.Margin = new System.Windows.Forms.Padding(4);
            this.txtDb.Name = "txtDb";
            this.txtDb.Size = new System.Drawing.Size(148, 29);
            this.txtDb.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(671, 293);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 18);
            this.label3.TabIndex = 39;
            this.label3.Text = "資料庫";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(402, 333);
            this.txtPwd.Margin = new System.Windows.Forms.Padding(4);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(148, 29);
            this.txtPwd.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(349, 347);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 37;
            this.label2.Text = "密碼";
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(120, 278);
            this.txtUrl.Margin = new System.Windows.Forms.Padding(4);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(540, 29);
            this.txtUrl.TabIndex = 35;
            // 
            // txtLoginId
            // 
            this.txtLoginId.Location = new System.Drawing.Point(123, 333);
            this.txtLoginId.Margin = new System.Windows.Forms.Padding(4);
            this.txtLoginId.Name = "txtLoginId";
            this.txtLoginId.Size = new System.Drawing.Size(148, 29);
            this.txtLoginId.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 293);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 18);
            this.label4.TabIndex = 33;
            this.label4.Text = "網址";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 349);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 34;
            this.label1.Text = "登入帳號";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.txtVerNote);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.txtVer);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Controls.Add(this.lstTobeUpdated);
            this.tabPage2.Controls.Add(this.btnImport);
            this.tabPage2.Controls.Add(this.btnGenTobeUpdate);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1001, 517);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "匯出並比對";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(443, 143);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(150, 28);
            this.label17.TabIndex = 13;
            this.label17.Text = "2.版本資訊";
            // 
            // txtVerNote
            // 
            this.txtVerNote.Location = new System.Drawing.Point(542, 256);
            this.txtVerNote.Name = "txtVerNote";
            this.txtVerNote.Size = new System.Drawing.Size(352, 29);
            this.txtVerNote.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(445, 268);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 18);
            this.label16.TabIndex = 11;
            this.label16.Text = "版本備註";
            // 
            // txtVer
            // 
            this.txtVer.Location = new System.Drawing.Point(542, 207);
            this.txtVer.Name = "txtVer";
            this.txtVer.Size = new System.Drawing.Size(191, 29);
            this.txtVer.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(445, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 18);
            this.label15.TabIndex = 9;
            this.label15.Text = "版本序號";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFromSource, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtToSource, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtTobeUpdate, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(18, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 104);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(619, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(175, 18);
            this.label14.TabIndex = 5;
            this.label14.Text = "修正後實際匯入檔案:";
            // 
            // txtFromSource
            // 
            this.txtFromSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFromSource.Location = new System.Drawing.Point(3, 23);
            this.txtFromSource.Multiline = true;
            this.txtFromSource.Name = "txtFromSource";
            this.txtFromSource.Size = new System.Drawing.Size(302, 78);
            this.txtFromSource.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(311, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 18);
            this.label13.TabIndex = 2;
            this.label13.Text = "被更新端原始檔:";
            // 
            // txtToSource
            // 
            this.txtToSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtToSource.Location = new System.Drawing.Point(311, 23);
            this.txtToSource.Multiline = true;
            this.txtToSource.Name = "txtToSource";
            this.txtToSource.Size = new System.Drawing.Size(302, 78);
            this.txtToSource.TabIndex = 1;
            // 
            // txtTobeUpdate
            // 
            this.txtTobeUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTobeUpdate.Location = new System.Drawing.Point(619, 23);
            this.txtTobeUpdate.Multiline = true;
            this.txtTobeUpdate.Name = "txtTobeUpdate";
            this.txtTobeUpdate.Size = new System.Drawing.Size(303, 78);
            this.txtTobeUpdate.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(302, 18);
            this.label12.TabIndex = 0;
            this.label12.Text = "來源端原始檔:";
            // 
            // lstTobeUpdated
            // 
            this.lstTobeUpdated.FormattingEnabled = true;
            this.lstTobeUpdated.Location = new System.Drawing.Point(21, 207);
            this.lstTobeUpdated.Name = "lstTobeUpdated";
            this.lstTobeUpdated.Size = new System.Drawing.Size(389, 292);
            this.lstTobeUpdated.TabIndex = 7;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(448, 329);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(222, 45);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "3.實際匯入";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnGenTobeUpdate
            // 
            this.btnGenTobeUpdate.Location = new System.Drawing.Point(18, 142);
            this.btnGenTobeUpdate.Name = "btnGenTobeUpdate";
            this.btnGenTobeUpdate.Size = new System.Drawing.Size(222, 45);
            this.btnGenTobeUpdate.TabIndex = 6;
            this.btnGenTobeUpdate.Text = "1.盤整實際匯入檔案";
            this.btnGenTobeUpdate.UseVisualStyleBackColor = true;
            this.btnGenTobeUpdate.Click += new System.EventHandler(this.btnGenTobeUpdate_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 28);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1001, 517);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "整理更新檔並匯入";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // lbTask
            // 
            this.lbTask.AutoSize = true;
            this.lbTask.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbTask.Location = new System.Drawing.Point(208, 575);
            this.lbTask.Name = "lbTask";
            this.lbTask.Size = new System.Drawing.Size(33, 24);
            this.lbTask.TabIndex = 25;
            this.lbTask.Text = "txt";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(8, 571);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(194, 28);
            this.label18.TabIndex = 24;
            this.label18.Text = "目前執行任務:";
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar1.Location = new System.Drawing.Point(0, 737);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1009, 36);
            this.progressBar1.TabIndex = 49;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 773);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lbTask);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtMessage);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToNg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToOk)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFromNG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromOk)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox picToNg;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.PictureBox picToOk;
        private System.Windows.Forms.Button btnSelectExportFolder;
        private System.Windows.Forms.TextBox txtExportFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DateTimePicker dtModified_on;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox picFromNG;
        private System.Windows.Forms.PictureBox picFromOk;
        private System.Windows.Forms.Button btnFromLogin;
        private System.Windows.Forms.TextBox txtFromDb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFromPwd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFromURL;
        private System.Windows.Forms.TextBox txtFromLoginId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtDb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.TextBox txtLoginId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtVerNote;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtVer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFromSource;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtToSource;
        private System.Windows.Forms.TextBox txtTobeUpdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckedListBox lstTobeUpdated;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnGenTobeUpdate;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label lbTask;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox chkExportAll;
    }
}

